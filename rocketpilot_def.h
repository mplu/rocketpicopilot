#define POS_MIN 0
#define POS_MAX 180
#define POS_INCR 10

#define SERVO_PIN 9

#define SEALEVELPRESSURE_HPA (1013.25)
#define UP 1
#define DOWN 0
