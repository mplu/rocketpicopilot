/***************************************************************************
  This is a library for the BMP3XX temperature & pressure sensor

  Designed specifically to work with the Adafruit BMP388 Breakout
  ----> http://www.adafruit.com/products/3966

  These sensors use I2C or SPI to communicate, 2 or 4 pins are required
  to interface.

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing products
  from Adafruit!

  Written by Limor Fried & Kevin Townsend for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
 ***************************************************************************/
/****************************************


additionnal doc :
https://github.com/earlephilhower/arduino-pico/tree/master?tab=readme-ov-file
https://learn.sparkfun.com/tutorials/using-the-serial-7-segment-display/all


*****************************************/


#include <Servo.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_BMP3XX.h"

#include "rocketpilot_def.h"


Servo myservo;  // create servo object to control a servo
int pos = 0;    // variable to store the servo position
char up_down = UP;

Adafruit_BMP3XX bmp;

// Here we'll define the I2C address of our S7S. By default it
//  should be 0x71. This can be changed, though.
const byte s7sAddress = 0x71;
unsigned int counter = 9900;  // This variable will count up to 65k
char tempString[10];  // Will be used with sprintf to create strings

int ledState = LOW;   // the current state of LED

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);

  for(int i = 0;i<100;i++)
  {
    // toggle state of LED
    ledState = !ledState;

    // control LED arccoding to the toggleed sate
    digitalWrite(LED_BUILTIN, ledState); 
    delay(50);
  }
  
  Serial.begin(115200);
  while (!Serial);
  Serial.println("Adafruit BMP388 / BMP390 test");

  InitBMP388();

  for(int i = 0;i<10;i++)
  {
    // toggle state of LED
    ledState = !ledState;

    // control LED arccoding to the toggleed sate
    digitalWrite(LED_BUILTIN, ledState); 
    delay(500);
  }

  Serial.println("Setup Display");
  InitI2C_Display();
  s7sSendStringI2C("-HI-");
  delay(500);
  // Clear the display before jumping into loop
  clearDisplayI2C();  
  
  InitMotor();

  Serial.println("end of setup");
}

void loop() {
  if (! bmp.performReading()) {
    Serial.println("Failed to perform reading :(");
    return;
  }
/*
  Serial.print("Temperature = ");
  Serial.print(bmp.temperature);
  Serial.println(" *C");

  Serial.print("Pressure = ");
  Serial.print(bmp.pressure / 100.0);
  Serial.println(" hPa");
*/
  float altitude_fl = bmp.readAltitude(SEALEVELPRESSURE_HPA);
  int altitude_int = (int)(altitude_fl * 10);
  Serial.print("Approx. Altitude = ");
  Serial.print(altitude_fl);
  Serial.println(" m");

  Serial.println();
  delay(100);


  sprintf(tempString, "%4d", altitude_int);
  s7sSendStringI2C(tempString);
  setDecimalsI2C(0b00000100);

  // toggle state of LED
  ledState = !ledState;

  // control LED arccoding to the toggleed sate
  digitalWrite(LED_BUILTIN, ledState); 
  

  if(up_down == UP)
  {
    pos+=POS_INCR;
  }else
  {
    pos-=POS_INCR;
  }

  if(pos>=POS_MAX)
  {
    up_down = DOWN;
    pos = POS_MAX;
  }
  if(pos<=POS_MIN)
  {
    up_down = UP;
    pos = POS_MIN;
  }
  myservo.write(pos);

}


